from room import Room
from character import Enemy
from character import Character
from item import Item       

kitchen = Room('kitchen')
#kitchen.set_name('kitchen')
#kitchen.get_name()
kitchen.set_description("A dank and dirty room buzzing with flies.")
#kitchen.describe()

dininghall = Room('Dining Hall')
dininghall.set_description("A Large room with golden decorations on each wall.")

ballroom = Room('Ball Room')
ballroom.set_description("A vast room with a shiny wooden floor.")

kitchen.link_room(dininghall,"south")
dininghall.link_room(kitchen,"north")
dininghall.link_room(ballroom,"west")
ballroom.link_room(dininghall,"east")


#Adding Enemy/Character in the room
dave = Enemy("Dave","A smelly Zombie")
dave.set_conversation("Hello! I am a Zombie. I like human blood, Rhhh!")
#dave.talk()

dave.set_weakness("cheese")
catrina = Character("Catrina","A Friendly skeleton")
catrina.set_conversation("Hello There!")

#Adding Item in the room
cheese = Item("cheese")
cheese.set_description("A large and smelly block of cheese!")
ballroom.set_item(cheese)



dininghall.set_character(dave)
current_room = kitchen   
backpack = []       

dead = False
while dead == False:		
    print("\n")         
    current_room.get_details()   

    #Check whether a character exit in the room or not if exit,describe
    hascharacter = current_room.get_character()
    if hascharacter is not None:
        hascharacter.describe()

    #Check whether an item exit in the room or not if exit,describe
    hasitem = current_room.get_item()
    if hasitem is not None:
        hasitem.describe()

    command = input("Enter action: ") 

    if command in ["north", "south", "east", "west"]:
        current_room = current_room.move(command)
    elif command == "talk":
        if hascharacter is not None:
            hascharacter.talk()
    elif command == "fight":
        if hascharacter is not None:
            item = input(print("Enter fight item: "))

            # Do I have the fight item
            if item in backpack:
                item = backpack[0]
            else:
                print("Item not found!")
                item = None
            if hascharacter.fight(item) == True:
                print("You Won!")
                print("There is no enemy in the room")
                current_room.set_character(None)
                if hascharacter.get_enemies_defeated == 2:
                    print("Congratulations, you have vanquished the enemy horde!")
                    dead = True
            else:
                print("Game Over!")
                dead = True
    elif command == "take":
        if hasitem is not None:
            backpack.append(hasitem.get_name())
            print("You put the " , hasitem.get_name(), " in your backpack.")
            current_room.set_item(None)
        



    


#All Done!


